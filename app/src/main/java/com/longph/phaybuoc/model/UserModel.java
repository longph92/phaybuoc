package com.longph.phaybuoc.model;

/**
 * Created by longphan on 11/23/16.
 */

public class UserModel {
    private String id;
    private String name;
    private String avatar;
    private String link;
    private String birthday;
    private Location location;

    public UserModel(String id, String name, String avatar, String link, String birthday, Location location) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.link = link;
        this.birthday = birthday;
        this.location = location;
    }

    public UserModel(String id, String name, String link, String birthday, Location location) {
        this.id = id;
        this.name = name;
        this.link = link;
        this.birthday = birthday;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public class Location {
        private String id;
        private String name;

        public Location(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
