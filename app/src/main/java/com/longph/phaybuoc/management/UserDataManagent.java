package com.longph.phaybuoc.management;

import android.content.Context;

import com.longph.phaybuoc.key.KeySharePreference;
import com.longph.phaybuoc.model.UserModel;
import com.longph.phaybuoc.utils.SharePreferenceUtils;
import com.longph.phaybuoc.utils.Utils;

import org.json.JSONException;

import java.lang.ref.WeakReference;

/**
 * Created by longphan on 11/23/16.
 */

public class UserDataManagent {
    private static UserDataManagent instance;
    private WeakReference<Context> mContextReference;

    private UserModel userData;

    public static synchronized void init(Context context) {
        if (instance == null) {
            instance = new UserDataManagent(context);
        }
    }

    public static synchronized UserDataManagent getInstance() {
        return instance;
    }

    private UserDataManagent(Context context) {
        this.mContextReference = new WeakReference<>(context);
    }

    public void setUser(UserModel data) {
        this.userData = data;
    }

    public UserModel getUser() {
        return this.userData;
    }

    public void storeUserData(String data) {
        SharePreferenceUtils.putString(mContextReference.get(), KeySharePreference.KEY_USER_DATA, data);
        try {
            setUser(Utils.parseResponseToModel(data, UserModel.class));
        } catch (JSONException e) {
            return;
        }
    }

    public String getUserData() {
        return SharePreferenceUtils.getString(mContextReference.get(), KeySharePreference.KEY_USER_DATA, "");
    }

    public boolean isUserAvailable() {
        return !getUserData().isEmpty();
    }

    public void cleanData() {
        SharePreferenceUtils.putString(mContextReference.get(), KeySharePreference.KEY_USER_DATA, "");
        clean();
    }

    public void clean() {
        this.userData = null;
    }

    public void reuse() {
        try {
            setUser(Utils.parseResponseToModel(getUserData(), UserModel.class));
        } catch (JSONException e) {
            return;
        }
    }
}
