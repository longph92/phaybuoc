package com.longph.phaybuoc.application;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.longph.phaybuoc.management.UserDataManagent;

/**
 * Created by longphan on 11/23/16.
 */

public class PBApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        UserDataManagent.init(this);
        UserDataManagent.getInstance().reuse();
//        AppEventsLogger.activateApp(this);
    }
}
