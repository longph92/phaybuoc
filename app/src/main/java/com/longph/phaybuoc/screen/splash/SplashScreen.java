package com.longph.phaybuoc.screen.splash;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.longph.phaybuoc.screen.main.MainActivity;
import com.longph.phaybuoc.R;
import com.longph.phaybuoc.screen.home.HomeActivity;
import com.longph.phaybuoc.management.UserDataManagent;
import com.longph.phaybuoc.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Long on 11/23/2016.
 */

public class SplashScreen extends FragmentActivity {

    private int DELAY = 1500;
    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startTiming();
    }

    private void startTiming() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startMainFlow();
            }
        }, DELAY);
    }

    private void startMainFlow() {
        if (UserDataManagent.getInstance().isUserAvailable()) {
            Utils.startActivitySingle(this, HomeActivity.class);
        } else {
            Utils.startActivitySingle(this, MainActivity.class);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }
}
