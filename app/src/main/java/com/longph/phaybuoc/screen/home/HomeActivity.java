package com.longph.phaybuoc.screen.home;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.longph.phaybuoc.R;
import com.longph.phaybuoc.management.UserDataManagent;
import com.longph.phaybuoc.screen.main.MainActivity;
import com.longph.phaybuoc.utils.Utils;
import com.squareup.picasso.Picasso;

/**
 * Created by Long on 11/23/2016.
 */

public class HomeActivity extends FragmentActivity {

    private static final int SELECT_PHOTO = 10001;
    private PopupWindow popUpInfoUser;
    private View popupView;
    private ShareDialog shareDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
    }

    private void initViews() {
        Picasso.with(this).load("https://graph.facebook.com/v2.5/" + UserDataManagent.getInstance().getUser().getId() + "/picture").into(((ImageView) findViewById(R.id.img_avatar)));
        Picasso.with(this).load("https://graph.facebook.com/v2.5/" + UserDataManagent.getInstance().getUser().getId() + "/picture").into(((ImageView) findViewById(R.id.img_avatar_feed)));
        Picasso.with(this).load("https://www.topcv.vn/company_logos/anpsoft-57982df911bfb.jpg").into(((ImageView) findViewById(R.id.img_content)));

        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        popupView = layoutInflater.inflate(R.layout.popup_info_user, null);

        ((TextView) popupView.findViewById(R.id.txt_name)).setText(UserDataManagent.getInstance().getUser().getName());
        ((TextView) popupView.findViewById(R.id.txt_birthday)).setText(UserDataManagent.getInstance().getUser().getBirthday());
        ((TextView) popupView.findViewById(R.id.txt_location)).setText(UserDataManagent.getInstance().getUser().getLocation().getName());

        popupView.findViewById(R.id.txt_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popUpInfoUser != null && popUpInfoUser.isShowing()) {
                    popUpInfoUser.dismiss();
                }
                Utils.getOpenFacebookIntent(HomeActivity.this, UserDataManagent.getInstance().getUser().getLink(), UserDataManagent.getInstance().getUser().getId());
            }
        });

        popupView.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (popUpInfoUser != null && popUpInfoUser.isShowing()) {
                    popUpInfoUser.dismiss();
                }
            }
        });

        popUpInfoUser = new PopupWindow(
                popupView,
                getResources().getDimensionPixelSize(R.dimen.size_popup_width),
                getResources().getDimensionPixelSize(R.dimen.size_popup_height));

        shareDialog = new ShareDialog(this);
    }

    public void onClickAvatar(View view) {
        popUpInfoUser.showAtLocation(view, Gravity.CENTER, (int) findViewById(R.id.img_avatar).getX(), (int) findViewById(R.id.img_avatar).getY());
    }

    public void onClickPostPhoto(View view) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    public void onClickShare(View view) {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("http://www.anpsoft.com"))
                .build();
        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
    }

    public void onClickAlbum(View view) {
        ((EditText) findViewById(R.id.edt_status)).setError(null);

        if (((EditText) findViewById(R.id.edt_status)).getText().length() > 0) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(((EditText) findViewById(R.id.edt_status)).getText().toString())
                    .setContentDescription(
                            "This is a demo made by myself. References:")
                    .setContentUrl(Uri.parse("https://www.google.com"))
                    .build();
            shareDialog.show(linkContent, ShareDialog.Mode.AUTOMATIC);
        } else {
            ((EditText) findViewById(R.id.edt_status)).setError("Should not empty.");
        }
    }

    public void onClickLogOut(View view) {
        LoginManager.getInstance().logOut();
        UserDataManagent.getInstance().cleanData();
        Utils.startActivitySingle(this, MainActivity.class);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            Bitmap bitmap = Utils.getBitmapFromIntent(this, data);
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(bitmap)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);
        }
    }

    @Override
    public void onBackPressed() {
        if (popUpInfoUser != null && popUpInfoUser.isShowing()) {
            popUpInfoUser.dismiss();
        } else {
            super.onBackPressed();
        }
    }
}
