package com.longph.phaybuoc.screen.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.longph.phaybuoc.R;
import com.longph.phaybuoc.screen.home.HomeActivity;
import com.longph.phaybuoc.management.UserDataManagent;
import com.longph.phaybuoc.utils.ToastMessage;
import com.longph.phaybuoc.utils.Utils;

import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends FragmentActivity {

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setListeners();
    }

    private void setListeners() {
        callbackManager = CallbackManager.Factory.create();

        ((LoginButton) findViewById(R.id.login_button)).setReadPermissions(Arrays.asList("user_location", "user_birthday, user_posts "));
        ((LoginButton) findViewById(R.id.login_button)).registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                fetchUserData(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                ToastMessage.show(MainActivity.this, "onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                ToastMessage.show(MainActivity.this, error.getMessage());
            }
        });
    }

    private void fetchUserData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        UserDataManagent.getInstance().storeUserData(object.toString());
                        Utils.startActivitySingle(MainActivity.this, HomeActivity.class);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,birthday,location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
