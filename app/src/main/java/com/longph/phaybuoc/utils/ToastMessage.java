package com.longph.phaybuoc.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Long on 11/23/2016.
 */

public class ToastMessage {
    public static void show(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
