package com.longph.phaybuoc.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by longphan on 11/23/16.
 */

public class SharePreferenceUtils {

    private static SharedPreferences getSharedPreferences(final Context context) {
        if (context == null) return null;
        return android.preference.PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isFirstTime(Context context, String key) {
        if (getBoolean(context, key, false)) {
            return false;
        } else {
            putBoolean(context, key, true);
            return true;
        }
    }

    public static boolean contains(Context context, String key) {
        return SharePreferenceUtils.getSharedPreferences(context).contains(key);
    }

    public static int getInt(final Context context, final String key, final int defaultValue) {
        return SharePreferenceUtils.getSharedPreferences(context).getInt(key, defaultValue);
    }

    public static boolean putInt(final Context context, final String key, final int pValue) {
        final SharedPreferences.Editor editor = SharePreferenceUtils.getSharedPreferences(context).edit();
        editor.putInt(key, pValue);
        return editor.commit();
    }

    public static long getLong(final Context context, final String key, final long defaultValue) {
        return SharePreferenceUtils.getSharedPreferences(context).getLong(key, defaultValue);
    }

    public static Long getLong(final Context context, final String key, final Long defaultValue) {
        if (SharePreferenceUtils.getSharedPreferences(context).contains(key)) {
            return SharePreferenceUtils.getSharedPreferences(context).getLong(key, 0);
        } else {
            return null;
        }
    }

    public static boolean putLong(final Context context, final String key, final long pValue) {
        final SharedPreferences.Editor editor = SharePreferenceUtils.getSharedPreferences(context).edit();
        editor.putLong(key, pValue);
        return editor.commit();
    }

    public static boolean getBoolean(final Context context, final String key, final boolean defaultValue) {
        return SharePreferenceUtils.getSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static boolean putBoolean(final Context context, final String key, final boolean pValue) {
        final SharedPreferences.Editor editor = SharePreferenceUtils.getSharedPreferences(context).edit();
        editor.putBoolean(key, pValue);
        return editor.commit();
    }

    public static String getString(final Context context, final String key, final String defaultValue) {
        return SharePreferenceUtils.getSharedPreferences(context).getString(key, defaultValue);
    }

    public static boolean putString(final Context context, final String key, final String pValue) {
        final SharedPreferences.Editor editor = SharePreferenceUtils.getSharedPreferences(context).edit();
        editor.putString(key, pValue);
        return editor.commit();
    }

    public static boolean remove(final Context context, final String key) {
        final SharedPreferences.Editor editor = SharePreferenceUtils.getSharedPreferences(context).edit();
        editor.remove(key);
        return editor.commit();
    }

    public static boolean saveArray(String[] array, String arrayName, Context mContext) {
        SharedPreferences prefs = getSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(arrayName + "_size", array.length);
        for (int i = 0; i < array.length; i++)
            editor.putString(arrayName + "_" + i, array[i]);
        return editor.commit();
    }

    public static String[] loadArray(String arrayName, Context mContext) {
        SharedPreferences prefs = getSharedPreferences(mContext);
        int size = prefs.getInt(arrayName + "_size", 0);
        String array[] = new String[size];
        for (int i = 0; i < size; i++)
            array[i] = prefs.getString(arrayName + "_" + i, null);
        return array;
    }
}
